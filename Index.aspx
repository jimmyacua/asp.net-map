﻿<html>
<head>
    <meta charset="utf-8" />
    <title>Ufinet Maps</title>

    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/jquery-3.0.0.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>

    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css" />
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>


    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v3&libraries=places&key=AIzaSyC95-pydxqm-eVgvGpR_SZ6pAH3n0AgnvE"></script>
    <script src="js/locationpicker.jquery.js"></script>
    <link rel="stylesheet" type="text/css" href="Index.css">
</head>

<body>
    <form id="form1" runat="server">

        <h1>Ufinet Maps</h1>

        <div class="menu" id="Dropdown" style="display: none">
            <b>Modo de viaje: </b>
            <select id="mode">
                <option value="DRIVING">En automóvil</option>
                <option value="WALKING">Caminando</option>
            </select>
        </div>

        <div class="container">
            <div class="card-title">
                <b>Ubicación de origen: </b>
            </div>
            <div class="col-form-label">
                <button type="button" data-toggle="modal" data-target="#ModalMapOrigin" class="btn btn-default">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <span id="ubicacionOrigin">Seleccionar ubicación origen</span>
                </button>
            </div>
            <div class="clearfix"></div>
            <div class="card-title">
                <b>Ubicación de destino: </b>
            </div>
            <div>
                <button type="button" data-toggle="modal" data-target="#ModalMapDest" class="btn btn-default">
                    <span class="glyphicon glyphicon-map-marker"></span>
                    <span id="ubicacionDest">Seleccionar ubicación destino</span>
                </button>
            </div>
            <div>
                <button id="calcRoutesButton" type="button" class="btn btn-default" style="background: dodgerblue; color: white; margin-top: 5%; margin-left: 4%;">
                    <span class="glyphicon glyphicon-play-circle"></span>
                    <span id="calcRoutes">Calcular rutas</span>
                </button>
            </div>

            <div>
                <p id="finalText" style="color: black; margin-top: 7%; display: none">text</p>
            </div>

            <style>
                .pac-container {
                    z-index: 99999;
                }
            </style>

            <div class="modal fade" id="ModalMapOrigin" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body" id="frameMapOrigin">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ubicación:</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="OriginAddress" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div id="ModalMapPreviewOrigin" style="width: 100%; height: 400px;"></div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="mx-auto">
                                        <label class="p-r-small col-sm-1 control-label">Lat:</label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="ModalMapLat" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="mx-auto">
                                        <label class="p-r-small col-sm-1 control-label">Long:</label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="ModalMapLon" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Aceptar</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="modal fade" id="ModalMapDest" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body" id="frameMapDest">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Ubicación:</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="destAddress" CssClass="form-control" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="col-sm-1">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div id="ModalMapPreviewDest" style="width: 100%; height: 400px;"></div>
                                    <div class="clearfix">&nbsp;</div>
                                    <div class="mx-auto">
                                        <label class="p-r-small col-sm-1 control-label">Lat:</label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="ModalMapLatDest" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="mx-auto">
                                        <label class="p-r-small col-sm-1 control-label">Long:</label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="ModalMapLonDest" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-primary btn-block" data-dismiss="modal">Aceptar</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div id="mapa" class="mapaGoogle" style="display: none">
        </div>

        <script type="text/javascript">
            var divMap = document.getElementById("mapa");
            navigator.geolocation.getCurrentPosition(fn_ok, fn_error);

            function fn_ok(response) {
                var lat = response.coords.latitude;
                var lon = response.coords.longitude;

                var isOriginSet = false;
                var isDestinationSet = false;

                var originCenter = null;
                var destCenter = null

                var latOrigin = null;
                var lonOrigin = null;

                var latDest = null;
                var lonDest = null;

                var gMarker = null;
                var gMarkerDest = null;
                var gMap = new google.maps.Map(divMap);


                $('#ModalMapPreviewOrigin').locationpicker({
                    radius: 0,
                    location: {
                        latitude: lat,
                        longitude: lon,
                    },
                    enableAutocomplete: true,
                    inputBinding: {
                        latitudeInput: $('#<%=ModalMapLat.ClientID%>'),
                        longitudeInput: $('#<%=ModalMapLon.ClientID%>'),
                        locationNameInput: $('#<%=OriginAddress.ClientID%>') //instrucción asp
                    },
                    onchanged: function (currentLocation, radius, isMarkerDropped) {
                        $('#ubicacionOrigin').html($('#<%=OriginAddress.ClientID%>').val());
                        latOrigin = $('#<%=ModalMapLat.ClientID%>').val();
                        lonOrigin = $('#<%=ModalMapLon.ClientID%>').val();
                        if (isOriginSet) {
                            gMarker.setMap(null);
                        }
                        isOriginSet = true;

                        if (isOriginSet) {
                            var gLatLong = new google.maps.LatLng(latOrigin, lonOrigin);
                            originCenter = gLatLong;

                            gMap.setOptions({
                                zoom: 15,
                                center: gLatLong
                            })

                            var objConfigMarker = {
                                position: gLatLong,
                                map: gMap,
                                draggable: false,
                                title: "Origen",
                                label: "O"
                            }
                            gMarker = new google.maps.Marker(objConfigMarker);
                            $("#mapa").show();
                        }
                    }
                });

                $('#ModalMapPreviewDest').locationpicker({
                    radius: 0,
                    location: {
                        latitude: lat,
                        longitude: lon,
                    },
                    enableAutocomplete: true,
                    inputBinding: {
                        latitudeInput: $('#<%=ModalMapLatDest.ClientID%>'),
                        longitudeInput: $('#<%=ModalMapLonDest.ClientID%>'),
                        locationNameInput: $('#<%=destAddress.ClientID%>') //instrucción asp
                    },
                    onchanged: function (currentLocation, radius, isMarkerDropped) {
                        $('#ubicacionDest').html($('#<%=destAddress.ClientID%>').val());
                        latDest = $('#<%=ModalMapLatDest.ClientID%>').val();
                        lonDest = $('#<%=ModalMapLonDest.ClientID%>').val();

                        if (isDestinationSet) {
                            gMarkerDest.setMap(null);
                        }

                        isDestinationSet = true;

                        if (isDestinationSet) {
                            var gLatLong = new google.maps.LatLng(latDest, lonDest);
                            destCenter = gLatLong
                            gMap.setOptions({
                                zoom: 15,
                                center: gLatLong,
                            })

                            var objConfigMarker = {
                                position: gLatLong,
                                map: gMap,
                                draggable: false,
                                title: "Destino",
                                label: "D"
                            }
                            gMarkerDest = new google.maps.Marker(objConfigMarker);
                            $("#mapa").show();
                        }
                    }
                });

                $("#ModalMapOrigin").on("show.bs.modal", function () {
                    $('#ModalMapPreviewOrigin').locationpicker("autosize");
                });
                $("#ModalMapDest").on("show.bs.modal", function () {
                    $('#ModalMapPreviewDest').locationpicker("autosize");
                });

                var gCoder = new google.maps.Geocoder();

                var objInfo = {
                    address: 'WWPX+QH San Pedro, San José, Montes de Oca'
                }

                gCoder.geocode(objInfo, fnCoder);

                function fnCoder(data) {
                    var coordenates = data[0].geometry.location; //obj Lat Long
                    var config = {
                        map: gMap,
                        position: coordenates,
                        draggable: true,
                    }

                    var gDirectionService = new google.maps.DirectionsService();

                    var directionsDisplays = [];
                    var infoWindows = [];
                    var rectangles = [];

                    var dangerLevels = ["Medio", "Alto"];
                    var colors = ["#FFA500", "#FF6347"]

                    displacement = [0.001, 0.0001, 0.002, 0.002, -0.001, -0.0001, -0.002, -0.002];

                    document.getElementById("calcRoutesButton").addEventListener("click", function () {
                        document.getElementById('finalText').innerHTML = "";
                        //for (var i = 0; i < infoWindows.length; i++) {
                        //    try {
                        //        infoWindows[i].close();
                        //        infoWindows.splice(i)
                        //    }
                        //    catch (e) {
                        //        console.log(e)
                        //    }
                        //}
                        //infoWindows = []
                        //console.log("click")
                        infoWindow1.close()
                        infoWindow2.close()
                        infoWindow3.close()
                        for (var j = 0; j < directionsDisplays.length; j++) {
                            directionsDisplays[j].setMap(null);
                            directionsDisplays.splice(j)
                        }
                        directionsDisplays = []

                        if (isOriginSet && isDestinationSet) {
                            for (var i = 0; i < rectangles.length; i++) {
                                rectangles[i].setMap(null);
                            }
                            rectangles = []
                            for (var i = 0; i < 5; i++) {
                                center = null;
                                if (Math.random() > 0.5) {
                                    center = destCenter
                                } else {
                                    center = originCenter
                                }

                                despLat = displacement[Math.round(Math.random() * (displacement.length - 0))];
                                var latC = center.lat() + despLat;
                                despLon = displacement[Math.round(Math.random() * (displacement.length - 0))];
                                var lngC = center.lng() + despLon;
                                //center = new google.maps.LatLng(latC, lngC);  
                                center2 = new google.maps.LatLng(latC + 0.002, lngC + 0.002);
                                var boundsRect = new google.maps.LatLngBounds(
                                    center,
                                    center2
                                );
                                dangerLevel = Math.round(Math.random() * (1 - 0));
                                var rectangle = new google.maps.Rectangle({
                                    strokeColor: colors[dangerLevel],
                                    strokeOpacity: 0.8,
                                    strokeWeight: 2,
                                    fillColor: colors[dangerLevel],
                                    fillOpacity: 0.35,
                                    map: gMap,
                                    bounds: boundsRect,
                                    draggable: true
                                });
                                rectangles.push(rectangle)
                            }
                            $("#Dropdown").show()

                            calculateAndDisplayRoute(gDirectionService);
                        }
                        else if (!isOriginSet && isDestinationSet) {
                            alert("Debe seleccionar una ubicación de origen");
                        }
                        else if (isOriginSet && !isDestinationSet) {
                            alert("Debe seleccionar una ubicación de destino");
                        }
                        else {
                            alert("Debe seleccionar una ubicación de origen y destino");
                        }
                    });

                    document.getElementById('mode').addEventListener('change', function () {
                        document.getElementById('finalText').innerHTML = "";
                        infoWindow1.close()
                        infoWindow2.close()
                        infoWindow3.close()
                        //for (var i = 0; i < infoWindows.length; i++) {
                        //    try {
                        //        infoWindows[i].close();
                        //        infoWindows.splice(i)
                        //    }
                        //    catch (e) {
                        //        console.log(e)
                        //    }
                        //}
                        //infoWindows = []
                        for (var j = 0; j < directionsDisplays.length; j++) {
                            directionsDisplays[j].setMap(null);
                            directionsDisplays.splice(j)
                        }
                        directionsDisplays = []
                        calculateAndDisplayRoute(gDirectionService);
                    });

                    var colorsRoutes = ["#b847ff", "#4d47ff", "#47ff5c", "#cbff47", "#47ffe0"]
                    var finalText = ""
                    var infoWindow1 = new google.maps.InfoWindow();
                    var infoWindow2 = new google.maps.InfoWindow();
                    var infoWindow3 = new google.maps.InfoWindow();
                    //-------------------------------------------------------------------------
                    function calculateAndDisplayRoute(directionsService) {
                        finalText = ""
                        //console.log("funca")
                        var selectedMode = document.getElementById('mode').value;
                        directionsService.route({
                            origin: originCenter,
                            destination: destCenter,
                            provideRouteAlternatives: true,
                            travelMode: google.maps.TravelMode[selectedMode]

                        }, function fnRouter(results, status) {

                            //muestra la línea entre origen y destino
                            if (status == 'OK') {

                                //console.log("rutas", results)
                                len = results.routes.length;

                                for (i = 0; i < len; i++) {
                                    var gDirectionRender = new google.maps.DirectionsRenderer({
                                        suppressPolylines: false,
                                        map: gMap,
                                        directions: results,
                                        routeIndex: i,
                                        suppressMarkers: true,
                                        suppressInfoWindow: true,
                                        polylineOptions: {
                                            strokeColor: colorsRoutes[i]
                                        }
                                    });

                                    directionsDisplays.push(gDirectionRender);


                                    var directionsData = results.routes[i].legs[0];

                                    //console.log(results.routes[i].bounds)
                                    try {
                                        infoWindow1.setContent("Ruta #" + "1" + "<br>" + directionsData.distance.text + "<br>" + directionsData.duration.text + "<br>");
                                        infoWindow1.setPosition(results.routes[0].legs[0].steps[Math.round(Math.random() * (results.routes[0].legs[0].steps.length - 0) + 0)].end_location);
                                        infoWindow1.open(gMap);

                                        infoWindow2.setContent("Ruta #" + "2" + "<br>" + directionsData.distance.text + "<br>" + directionsData.duration.text + "<br>");
                                        infoWindow2.setPosition(results.routes[1].legs[0].steps[Math.round(Math.random() * (results.routes[2].legs[0].steps.length - 0) + 0)].end_location);
                                        infoWindow2.open(gMap);

                                        infoWindow3.setContent("Ruta #" + "3" + "<br>" + directionsData.distance.text + "<br>" + directionsData.duration.text + "<br>");
                                        infoWindow3.setPosition(results.routes[2].legs[0].steps[Math.round(Math.random() * (results.routes[2].legs[0].steps.length - 0) + 0)].end_location);
                                        infoWindow3.open(gMap);
                                    } catch (e) {
                                        console.log(e)
                                    }

                                }
                                var dangerColor = ""
                                var bound1 = [];
                                var bound2 = [];
                                for (var j = 0; j < rectangles.length; j++) {

                                    bound1.push(rectangles[j].bounds.Za["i"])//lat
                                    bound1.push(rectangles[j].bounds.Za["j"])//lat
                                    bound2.push(rectangles[j].bounds.Ua["i"])//lon
                                    bound2.push(rectangles[j].bounds.Ua["j"])//lon
                                    //console.log("bounds", bound1, "; ", bound2)
                                    var route = 0;
                                    var isIntersection = false;
                                    while (route < results.routes.length) {
                                        dangerColor = ""
                                        var m = 0;
                                        while (m < results.routes[route].legs[0].steps.length) {
                                            var n = 0;

                                            while (n < results.routes[route].legs[0].steps[m].path.length) {
                                                var routeLat = results.routes[route].legs[0].steps[m].path[n].lat();
                                                var routeLon = results.routes[route].legs[0].steps[m].path[n].lng();
                                                if ((bound1[0] <= routeLat && routeLat <= bound1[1]) && (bound2[0] <= routeLon && routeLon <= bound2[1])) {
                                                    isIntersection = true;
                                                    //console.log("rect", rectangles[j])
                                                    if (rectangles[j].fillColor == "#FF6347") {
                                                        dangerColor = "alta."
                                                    } else {
                                                        dangerColor = "media."
                                                    }
                                                }
                                                n++;
                                            }
                                            m++;
                                        }
                                        if (dangerColor != "") {
                                            finalText += "Ruta #" + (route + 1).toString() + " pasa por una zona con peligrosidad " + dangerColor + "<br>"
                                        }
                                        route++;
                                    }
                                    bound1 = []
                                    bound2 = []

                                }
                                document.getElementById('finalText').innerHTML = finalText;
                                $("#finalText").show();


                            } else {
                                alert('Error! No se encontraron rutas');
                            }
                            //alert(finalText)
                        });

                    }


                }
            }
            function fn_error() {
            }
        </script>
    </form>
</body>
</html>
